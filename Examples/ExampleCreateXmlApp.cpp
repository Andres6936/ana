#include "Ana.hpp"

using namespace Ana;

const unsigned int NUM_INDENTS_PER_SPACE = 2;

const char* getIndent(unsigned int numIndents)
{
	static const char* pINDENT = "                                      + ";
	static const unsigned int LENGTH = strlen(pINDENT);
	unsigned int n = numIndents * NUM_INDENTS_PER_SPACE;
	if (n > LENGTH)
	{ n = LENGTH; }

	return &pINDENT[LENGTH - n];
}

// same as getIndent but no "+" at the end
const char* getIndentAlt(unsigned int numIndents)
{
	static const char* pINDENT = "                                        ";
	static const unsigned int LENGTH = strlen(pINDENT);
	unsigned int n = numIndents * NUM_INDENTS_PER_SPACE;
	if (n > LENGTH)
	{ n = LENGTH; }

	return &pINDENT[LENGTH - n];
}

int dump_attribs_to_stdout(Element* pElement, unsigned int indent)
{
	if (!pElement)
	{ return 0; }

	Attribute* pAttrib = pElement->FirstAttribute();
	int i = 0;
	int ival;
	double dval;
	const char* pIndent = getIndent(indent);
	printf("\n");
	while (pAttrib)
	{
		printf("%s%s: value=[%s]", pIndent, pAttrib->Name(), pAttrib->Value());

		if (pAttrib->QueryIntValue(&ival) == TIXML_SUCCESS)
		{ printf(" int=%d", ival); }
		if (pAttrib->QueryDoubleValue(&dval) == TIXML_SUCCESS)
		{ printf(" d=%1.1f", dval); }
		printf("\n");
		i++;
		pAttrib = pAttrib->Next();
	}
	return i;
}

void dump_to_stdout(TiXmlNode* pParent, unsigned int indent = 0)
{
	if (!pParent)
	{ return; }

	TiXmlNode* pChild;
	Text* pText;
	int t = pParent->Type();
	printf("%s", getIndent(indent));
	int num;

	switch (t)
	{
	case TiXmlNode::DOCUMENT:
		printf("Document");
		break;

	case TiXmlNode::ELEMENT:
		printf("Element [%s]", pParent->GetValue());
		num = dump_attribs_to_stdout(pParent->ToElement(), indent + 1);
		switch (num)
		{
		case 0:
			printf(" (No attributes)");
			break;
		case 1:
			printf("%s1 attribute", getIndentAlt(indent));
			break;
		default:
			printf("%s%d attributes", getIndentAlt(indent), num);
			break;
		}
		break;

	case TiXmlNode::COMMENT:
		printf("Comment: [%s]", pParent->GetValue());
		break;

	case TiXmlNode::UNKNOWN:
		printf("Unknown");
		break;

	case TiXmlNode::TEXT:
		pText = pParent->ToText();
		printf("Text: [%s]", pText->GetValue());
		break;

	case TiXmlNode::DECLARATION:
		printf("Declaration");
		break;
	default:
		break;
	}
	printf("\n");
	for (pChild = pParent->FirstChild(); pChild != 0; pChild = pChild->NextSibling())
	{
		dump_to_stdout(pChild, indent + 1);
	}
}

int main(int argc, char** argv)
{
	Document doc;
	Declaration* decl = new Declaration("1.0", "", "");
	doc.LinkEndChild(decl);

	Element* root = new Element("MyApp");
	doc.LinkEndChild(root);

	Comment* comment = new Comment();
	comment->SetValue(" Settings for MyApp ");
	root->LinkEndChild(comment);

	Element* msgs = new Element("Messages");
	root->LinkEndChild(msgs);

	Element* msg = new Element("Welcome");
	msg->LinkEndChild(new Text("Welcome to MyApp"));
	msgs->LinkEndChild(msg);

	msg = new Element("Farewell");
	msg->LinkEndChild(new Text("Thank you for using MyApp"));
	msgs->LinkEndChild(msg);

	Element* windows = new Element("Windows");
	root->LinkEndChild(windows);

	Element* window = new Element("Window");
	windows->LinkEndChild(window);
	window->SetAttribute("name", "MainFrame");
	window->SetAttribute("x", 5);
	window->SetAttribute("y", 15);
	window->SetAttribute("w", 400);
	window->SetAttribute("h", 250);

	Element* cxn = new Element("Connection");
	root->LinkEndChild(cxn);
	cxn->SetAttribute("ip", "192.168.0.1");
	cxn->SetDoubleAttribute("timeout", 123.456); // floating point attrib

	dump_to_stdout(&doc);
	doc.SaveFile("AppSettings.xml");
}