#include "Ana.hpp"

using namespace Ana;

int main(int argc, char** argv)
{
	// Make xml: <?xml ..><Hello>World</Hello>
	Document doc;
	Declaration* decl = new Declaration("1.0", "", "");
	Element* element = new Element("Hello");
	Text* text = new Text("World");
	element->LinkEndChild(text);
	doc.LinkEndChild(decl);
	doc.LinkEndChild(element);
	doc.SaveFile("MadeByHand.xml");
}