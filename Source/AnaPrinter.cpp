#include "AnaPrinter.hpp"

using namespace Ana;

bool TiXmlPrinter::VisitEnter(const Document&)
{
	return true;
}

bool TiXmlPrinter::VisitExit(const Document&)
{
	return true;
}

bool TiXmlPrinter::VisitEnter(const Element& element, const Attribute* firstAttribute)
{
	DoIndent();
	buffer += "<";
	buffer += element.GetValue();

	for (const Attribute* attrib = firstAttribute; attrib; attrib = attrib->Next())
	{
		buffer += " ";
		attrib->Print(0, 0, &buffer);
	}

	if (!element.FirstChild())
	{
		buffer += " />";
		DoLineBreak();
	}
	else
	{
		buffer += ">";
		if (element.FirstChild()->ToText()
			&& element.LastChild() == element.FirstChild()
			&& element.FirstChild()->ToText()->CDATA() == false)
		{
			simpleTextPrint = true;
			// no DoLineBreak()!
		}
		else
		{
			DoLineBreak();
		}
	}
	++depth;
	return true;
}


bool TiXmlPrinter::VisitExit(const Element& element)
{
	--depth;
	if (!element.FirstChild())
	{
		// nothing.
	}
	else
	{
		if (simpleTextPrint)
		{
			simpleTextPrint = false;
		}
		else
		{
			DoIndent();
		}
		buffer += "</";
		buffer += element.GetValue();
		buffer += ">";
		DoLineBreak();
	}
	return true;
}


bool TiXmlPrinter::Visit(const Text& text)
{
	if (text.CDATA())
	{
		DoIndent();
		buffer += "<![CDATA[";
		buffer += text.GetValue();
		buffer += "]]>";
		DoLineBreak();
	}
	else if (simpleTextPrint)
	{
		std::string str;
		TiXmlBase::EncodeString(text.GetValueTStr(), &str);
		buffer += str;
	}
	else
	{
		DoIndent();
		std::string str;
		TiXmlBase::EncodeString(text.GetValueTStr(), &str);
		buffer += str;
		DoLineBreak();
	}
	return true;
}


bool TiXmlPrinter::Visit(const Declaration& declaration)
{
	DoIndent();
	declaration.Print(0, 0, &buffer);
	DoLineBreak();
	return true;
}


bool TiXmlPrinter::Visit(const Comment& comment)
{
	DoIndent();
	buffer += "<!--";
	buffer += comment.GetValue();
	buffer += "-->";
	DoLineBreak();
	return true;
}


bool TiXmlPrinter::Visit(const Unknown& unknown)
{
	DoIndent();
	buffer += "<";
	buffer += unknown.GetValue();
	buffer += ">";
	DoLineBreak();
	return true;
}

bool TiXmlPrinter::Visit(const StylesheetReference& stylesheet)
{
	DoIndent();
	stylesheet.Print(0, 0, &buffer);
	DoLineBreak();
	return true;
}